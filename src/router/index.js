import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Signup from '../views/Signup.vue';
import Signin from '../views/Signin.vue';
import Profile from '../views/User/Profile.vue';
import MyFavourites from '../views/User/MyFavourites.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      title: 'Home｜Hong Kong Aviation Society',
    },
  },
  {
    path: '/signup',
    name: 'Signup',
    meta: {
      title: 'Sign In｜Hong Kong Aviation Society',
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Signup,
  },
  {
    path: '/signin',
    name: 'Signin',
    meta: {
      title: 'Sign In｜Hong Kong Aviation Society',
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Signin,
  },
  {
    path: '/user/profile',
    name: 'Profile',
    meta: {
      title: 'Profile｜User｜Hong Kong Aviation Society',
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Profile,
  },
  {
    path: '/user/myfav',
    name: 'My Favourites',
    meta: {
      title: 'My Favourites｜User｜Hong Kong Aviation Society',
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: MyFavourites,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((toRoute, fromRoute, next) => {
  window.document.title = toRoute.meta && toRoute.meta.title ? toRoute.meta.title : 'Home｜Hong Kong Aviation Society';
  next();
});

export default router;
